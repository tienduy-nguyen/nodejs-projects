import { App } from './App';
import { connectDB } from './config/db';

const app = App.getInstance();

connectDB();

const PORT = 3000;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});

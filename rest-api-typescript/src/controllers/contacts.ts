import { Request, Response } from 'express';
import { Contact } from '../models/Contact';

export interface IContact {
  id: string;
  firstName: string;
  lastName: string;
  email: string;
  company: string;
  phone: number;
}

export interface RequestWithContact extends Request {
  body: IContact;
}
export class ContactController {
  constructor() {}

  //Create or update a contact
  public createOrUpdateContact = async (
    req: RequestWithContact,
    res: Response
  ) => {
    const newContact: IContact = req.body;

    try {
      let contact = await Contact.findOneAndUpdate(
        { email: newContact.email },
        { $set: newContact },
        { new: true, upsert: true, setDefaultsOnInsert: true }
      );

      res.json(contact);
    } catch (err) {
      console.error(err.message);
      res.status(500).send('Server error');
    }
  };

  //Get all contacts
  public getAllContacts = async (req: RequestWithContact, res: Response) => {
    try {
      const contacts = await Contact.find().sort({ createAt: -1 });
      res.json(contacts);
    } catch (err) {
      console.log(err.message);
      res.status(500).send('Server error');
    }
  };

  //Show single contact
  public getContactById = async (req: RequestWithContact, res: Response) => {
    try {
      const contact = await Contact.findById(req.params.id);
      if (!contact) {
        res.status(404).json({ message: 'Contact not found' });
        return;
      }
      res.json(contact);
    } catch (err) {
      console.error(err.message);
      res.status(500).send('Server error');
    }
  };

  // Delete contact
  public deleteContactById = async (req: RequestWithContact, res: Response) => {
    try {
      const contact = await Contact.findById(req.params.id);
      if (!contact) {
        res.status(404).json({ message: 'Contact not found' });
        return;
      }
      await contact.remove();
      res.json({ message: 'Contact removed' });
    } catch (err) {
      console.error(err.message);
      res.status(500).send('Send error');
    }
  };
}

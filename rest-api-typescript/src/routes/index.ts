import express, { Request, Response, NextFunction, Router } from 'express';
import { ContactRouter } from './contact';

export class AppRouter {
  static _instance: Router;

  static getInstance(): Router {
    if (!AppRouter._instance) {
      AppRouter._instance = express.Router();
    }
    return AppRouter._instance;
  }

  public static initRoutes(app: express.Application): void {
    app.use('/api/contacts', new ContactRouter().router);
  }
}

import express, { Request, Response, NextFunction, Router } from 'express';
import { AppRouter } from './index';
import { ContactController } from '../controllers/contacts';

export class ContactRouter {
  public router: Router;
  constructor() {
    this.router = AppRouter.getInstance();
    this._initRoutes();
  }
  private _initRoutes(): void {
    const contactController = new ContactController();
    // @route    POST '/api/contacts'
    // @desc     Create new contact
    // @access   public
    this.router.post('/', contactController.createOrUpdateContact);

    // @route    GET api/contacts
    // @access   public
    this.router.get('/', contactController.getAllContacts);

    // @route    GET api/contacts/:id
    // @access   public
    this.router.get('/:id', contactController.getContactById);

    // @route    DELETE api/contacts/:id
    // @access   public
    this.router.delete('/:id', contactController.deleteContactById);
  }
}

import express, { Application, Router } from 'express';
import bodyParser from 'body-parser';
import { AppRouter } from './routes';

export class App {
  private static _instance: Application;

  static getInstance(): Application {
    if (!App._instance) {
      App._instance = express();

      App.config();
    }
    return App._instance;
  }
  private static config(): void {
    const app: Application = App._instance;
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: true }));

    // Define routes
    AppRouter.initRoutes(app);
  }
}

import mongoose from 'mongoose';
import * as dotenv from 'dotenv';
dotenv.config();
const db = process.env.MONGO_URI as string;

export const connectDB = async () => {
  try {
    await mongoose.connect(db, {
      useFindAndModify: false,
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
    });
    console.log('MongoDB has been connected');
  } catch (err) {
    console.log(err.message);
    //Exit process with failure
    process.exit(1);
  }
};

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const App_1 = require("./App");
const db_1 = require("./config/db");
const app = App_1.App.getInstance();
db_1.connectDB();
const PORT = 3000;
app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
});

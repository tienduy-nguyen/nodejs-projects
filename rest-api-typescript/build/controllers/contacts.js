"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ContactController = void 0;
const Contact_1 = require("../models/Contact");
class ContactController {
    constructor() {
        //Create or update a contact
        this.createOrUpdateContact = (req, res) => __awaiter(this, void 0, void 0, function* () {
            const newContact = req.body;
            try {
                let contact = yield Contact_1.Contact.findOneAndUpdate({ email: newContact.email }, { $set: newContact }, { new: true, upsert: true, setDefaultsOnInsert: true });
                res.json(contact);
            }
            catch (err) {
                console.error(err.message);
                res.status(500).send('Server error');
            }
        });
        //Get all contacts
        this.getAllContacts = (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const contacts = yield Contact_1.Contact.find().sort({ createAt: -1 });
                res.json(contacts);
            }
            catch (err) {
                console.log(err.message);
                res.status(500).send('Server error');
            }
        });
        //Show single contact
        this.getContactById = (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const contact = yield Contact_1.Contact.findById(req.params.id);
                if (!contact) {
                    res.status(404).json({ message: 'Contact not found' });
                    return;
                }
                res.json(contact);
            }
            catch (err) {
                console.error(err.message);
                res.status(500).send('Server error');
            }
        });
        // Delete contact
        this.deleteContactById = (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const contact = yield Contact_1.Contact.findById(req.params.id);
                if (!contact) {
                    res.status(404).json({ message: 'Contact not found' });
                    return;
                }
                yield contact.remove();
                res.json({ message: 'Contact removed' });
            }
            catch (err) {
                console.error(err.message);
                res.status(500).send('Send error');
            }
        });
    }
}
exports.ContactController = ContactController;

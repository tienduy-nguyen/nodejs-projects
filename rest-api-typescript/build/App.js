"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.App = void 0;
const express_1 = __importDefault(require("express"));
const body_parser_1 = __importDefault(require("body-parser"));
const routes_1 = require("./routes");
class App {
    static getInstance() {
        if (!App._instance) {
            App._instance = express_1.default();
            App.config();
        }
        return App._instance;
    }
    static config() {
        const app = App._instance;
        app.use(body_parser_1.default.json());
        app.use(body_parser_1.default.urlencoded({ extended: true }));
        // Define routes
        routes_1.AppRouter.initRoutes(app);
    }
}
exports.App = App;

"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppRouter = void 0;
const express_1 = __importDefault(require("express"));
const contact_1 = require("./contact");
class AppRouter {
    static getInstance() {
        if (!AppRouter._instance) {
            AppRouter._instance = express_1.default.Router();
        }
        return AppRouter._instance;
    }
    static initRoutes(app) {
        app.use('/api/contacts', new contact_1.ContactRouter().router);
    }
}
exports.AppRouter = AppRouter;

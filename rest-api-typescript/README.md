# Building RESTful Web APIs with Node.js, Express, MongoDB and TypeScript

- [Building RESTful Web APIs with Node.js, Express, MongoDB and TypeScript](#building-restful-web-apis-with-nodejs-express-mongodb-and-typescript)
  - [Setting up project](#setting-up-project)
  - [Reference](#reference)

## Setting up project

- Initiate a Node project
  ```bash
  $ mkdir rest-api-typescript
  $ cd rest-api-typescript
  $ npm init -y
  ```
- Install all dependencies
  ```bash
  $ npm i --save @types/express express body-parser mongoose nodemon
  ```
- Configure the typescript configuration file: `tsconfig.json`
  ```json
  {
    "compilerOptions": {
          "module": "commonjs",
          "moduleResolution": "node",
          "pretty": true,
          "sourceMap": true,
          "target": "es6",
          "outDir": "./build",
          "baseUrl": "./src",
          "moduleResolution": "node", 
          "experimentalDecorators": true, 
          "emitDecoratorMetadata": true, 
      },
      "include": [
          "src/**/*.ts"
      ],
      "exclude": [
          "node_modules"
      ]
  }
  ```
- Edit the running scripts in package.json
  ```json
  {
      "scripts": {
        "build": "tsc -w",
        "dev": "ts-node ./src/index.ts",
        "start": "nodemon ./build/index.js",
        "prod": "npm run build && npm run start"
      }
  }
  ```
- Getting started with the base configuration
  ```ts
  // src/app.ts

  import * as express from "express";
  import * as bodyParser from "body-parser";

  class App {

      public app: express.Application;

      constructor() {
          this.app = express();
          this.config();
      }

      private config(): void{
          // support application/json type post data
          this.app.use(bodyParser.json());
          //support application/x-www-form-urlencoded post data
          this.app.use(bodyParser.urlencoded({ extended: false }));
      }

  }

  export default new App().app;
  ```

  ```ts
  // src/server.ts

  import app from "./app";
  const PORT = 3000;

  app.listen(PORT, () => {
      console.log('Express server listening on port ' + PORT);
  })
  ```
## Reference

[Ref](https://restful-api-node-typescript.books.dalenguyen.me/en/latest/setting-up-project.html)
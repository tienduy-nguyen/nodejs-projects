app = angular.module('app.todos');
//Declare controller
app.controller('todoController', ['$scope', 'svTodos', function ($scope, svTodos) {
    //Declare a model
    $scope.appName = "Todos Dashboard";
    $scope.formData = {};
    $scope.loading = true;
    $scope.todos = [];

    svTodos.get().then(function(response){
        $scope.todos = response.data;
        console.log("status:" + response.status);
        $scope.loading =false;
    }, function (error){});
   
    $scope.createTodo = function () {
            $scope.loading = true;
        var todo = {
            text: $scope.formData.text,
            isDone: false
        }
        svTodos.create(todo)
            .then(function (response) {
                $scope.todos = response.data;
                $scope.formData.text = '';
            }, function (error){})
            $scope.loading = false;
    };
    $scope.updateTodo = function (todo) {
        console.log('Update: ', todo);
        $scope.loading = true;
        svTodos.update(todo).then(function(response){
            $scope.todos = response.data;
            $scope.loading = false;
        }, function (error){})
        
    };
    $scope.deleteTodo = function (todo) {
        console.log('Delete: ', todo);
        $scope.loading = true;
        svTodos.delete(todo._id).then(function(response){
            $scope.todos = response.data;
        }, function (error){})
        $scope.loading = false;
    };

    //when we set a variable in scope, we can use it in view
}]);
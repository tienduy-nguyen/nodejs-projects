const express = require('express');
const bodyParser = require('body-parser');
var path = require('path');
const morgan = require('morgan');
const mongoose = require('mongoose');
const config = require('./config');
const dotenv = require('dotenv');
dotenv.config();

var cors = require("cors");
const setupController = require('./api/controllers/setupController');
const todoController = require('./api/controllers/todoController');

const app = express();
const port = process.env.PORT || 3000;

//Su dung middleware de cund cap tai nguyen tinh cho nguoi dung
//__dirname la thu muc hien tai
//
app.use(cors());
app.use('/assets', express.static(__dirname + '/public'));
app.use(bodyParser.json()); //Kieu du lieu muon doc la json

app.use(bodyParser.urlencoded({ extended: true })); //middleware dataform

//Su dung tiep tuc middleware voi thu vien morgan, log moi request ra console
app.use(morgan('dev'));

// view engine setup
app.set('view engine', 'ejs');

//De application hieu folder views
//app.set('views','./views');


//Info Db
// console.log(config.getDbConnectionString());
const mongoURI = config.getDbConnectionString();
mongoose.set('useFindAndModify', false);
mongoose.connect(process.env.MONGODB_URI || mongoURI,
    {
        useUnifiedTopology: true,
        useNewUrlParser: true
    });
// console.log(config.getDbConnectionString());
mongoose.connection.once('open', () => { console.log('MongoDB Connected'); });
mongoose.connection.on('error', (err) => { console.log('MongoDB connection error: ', err); });
setupController(app);
todoController(app);

//Cau hinh cac dinh tuyen
app.get('/', function (req, res) {
    res.render('index');
})

// sau khi khoi dong thanh cong, chung ta se ghi ra dong thong bao duoi console
app.listen(port, function () {
    console.log('App listening on port :' + port);
})
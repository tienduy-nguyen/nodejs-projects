const Todos = require('../models/todoModel');

module.exports = function (app) {
    app.get('/api/setupTodos', function (req, res) {

        //Setup seed date
        const seedTodos = [{
                text: 'Learn Node.js',
                isDone: false
            },
            {
                text: 'Learn framework angular',
                isDone: 'false'
            },
            {
                text: 'Complete the application',
                isDone: 'false'
            }
        ];
        Todos.create(seedTodos, function (err, results) {
            res.send(results);
        })
    })
}
const express = require('express');
const app = express();
const dotenv = require('dotenv');
const bodyParser = require('body-parser');
const multer = require('multer');
// var upload = multer({ dest: 'uploads/' });

dotenv.config();
const userdb = process.env.DB_USER
const passdb = process.env.DB_PASS;
const { Client } = require('pg')
const connectionString = `postgresql://${userdb}:${passdb}@localhost:5432/MerryChristmas`;
const client = new Client({
    connectionString: connectionString
});
client.connect();

const port = process.env.PORT || 3000;


app.use(express.static('./public'));
app.use(express.static('./upload'));
app.set('view engine', 'ejs');
app.set('views', './views');
const server = require('http').Server(app);

// create application/json parser
const jsonParser = bodyParser.json()

// create application/x-www-form-urlencoded parser
const urlencodedParser = bodyParser.urlencoded({ extended: false })
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './upload')
    },
    filename: function (req, file, cb) {
        cb(null, file.originalname)
    }
})

var upload = multer({ storage: storage }).single('uploadfile');



app.get('/', function (req, res) {
    client.query("select * from video", (err, result) => {
        if (err) {
            res.status(400).send(err);
            return console.error('Error running query', err);
        }
        res.render('home', { data: result });
        // res.status(200).send(result.rows);
    });

})
app.get('/video/list', function (req, res) {
    client.query("select * from video", (err, result) => {
        if (err) {
            res.status(400).send(err);
            return console.error('Error running query', err);
        }
        res.render('list', { data: result });
    });
})

app.get('/video/edit/:id', function (req, res) {
    let id = req.params.id;
    client.query("select * from video where id=" + id, (err, result) => {
        if (err) {
            res.status(400).send(err);
            return console.error('Error running query', err);
        }
        res.render('edit', { data: result.rows[0] });
    });
})
app.post('/video/edit/:id', urlencodedParser, function (req, res) {
    let id = req.params.id;
    console.log(id);
    upload(req, res, function (err) {
        if (err) {
            // A Multer error occurred when uploading.
            return res.status(400).send("An unknown error occurred when uploading.");
        }
        let sql='';
        if (!req.file) {
            sql = `update video set title='${req.body.title}' ,description='${req.body.description}',key='${req.body.key}' where id=${id}`;
        }else{
            sql = `update video set title='${req.body.title}' ,description='${req.body.description}',key='${req.body.key}',image='${req.file.originalname}' where id=${id}`;
        }

        client.query(sql, (err, result) => {
            if (err) {
                res.status(400).send(err);
                return console.error('Error running query', err);
            }
            res.redirect('../list')
        });
    });
});



app.get('/video/add', (req, res) => {
    res.render('add');
});

app.post('/video/add', urlencodedParser, (req, res) => {
    upload(req, res, function (err) {
        if (err) {
            // A Multer error occurred when uploading.
            return res.status(400).send("An unknown error occurred when uploading.");
        }
        if (!req.file) {
            return  res.send('You need select a file to load');
        }
        let sql = `insert into video(title,description,key,image) values('${req.body.title}','${req.body.description}','${req.body.key}','${req.file.originalname}')`;
        // console.log(sql);
        client.query(sql, (err, result) => {
            if (err) {
                res.status(400).send(err);
                return console.error('Error running query', err);
            }
            res.redirect('./list')
        });
    });

});

app.get('/video/delete/:id', function (req, res) {
    client.query("delete from video where id=" + req.params.id, (err, result) => {
        if (err) {
            res.status(400).send(err);
            return console.error('Error running query', err);
        }
        // res.render('list', { data: result });
    });
    res.redirect('../list')
});




server.listen(port, function () {
    console.log('App listening on port: ', port);
});


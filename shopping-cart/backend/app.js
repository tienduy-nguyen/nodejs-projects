const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const logger = require('morgan');
const dotenv = require('dotenv');
const mongoose = require('mongoose');
const session = require('express-session');
const MongoDBStore = require('connect-mongodb-session')(session);
const csrf = require('csurf');
const flash = require('connect-flash');
const multer = require('multer');



const errorController = require('./controllers/error');
const User = require('./models/user');
const shopController = require('./controllers/shop');
const isAuth = require('./middleware/is-auth');


const app = express();
dotenv.config();
const port = process.env.PORT || 3000;


const store = new MongoDBStore({
    uri: process.env.MONGODB_URI,
    collection: 'session',
});
const csrfProtection = csrf();
const fileStorage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, 'uploads/');
    },
    filename: (req, file, cb) => {
        const uniquePrefix = Date.now(); //+ '-' + Math.round(Math.random() * 1E9);
        cb(null, uniquePrefix+ '-' + file.originalname)
    }
})

const fileFilter = (req, file, cb) => {
    if (
      file.mimetype === 'image/png' ||
      file.mimetype === 'image/jpg' ||
      file.mimetype === 'image/jpeg'
    ) {
      cb(null, true);
    } else {
      cb(null, false);
    }
  };

app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'views'));
const adminRoutes = require('./routes/admin');
const shopRoutes = require('./routes/shop');
const authRoutes = require('./routes/auth');


app.use(logger('dev'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(multer({ storage: fileStorage ,fileFilter:fileFilter}).single('image'));
app.use(express.static(path.join(__dirname, 'public')));
app.use('/uploads',express.static(path.join(__dirname, 'uploads')));

app.use(session({
    secret: 'mySecretSession',
    resave: false,
    saveUninitialized: false,
    store: store
}));


app.use(flash());

//https://stackoverflow.com/questions/31893794/forbiddenerror-invalid-csrf-token-express-js
app.use((req, res, next) => {
    res.locals.isAuthenticated = req.session.isLoggedIn;;
    next();
  });


app.use((req, res, next) => {
    if (!req.session.user) {
        return next();
    }
    User.findById(req.session.user._id)
        .then(user => {
            if (!user) {
                return next();
            }
            req.user = user;
            next();
        })
        .catch(err => {
            next(new Error(err));
        });
});

app.post('/create-order', isAuth,shopController.postOrder);

app.use(csrfProtection);
app.use((req, res, next) => {
    res.locals.csrfToken = req.csrfToken();
    next();
  });

app.use('/admin', adminRoutes);
app.use(shopRoutes);
app.use(authRoutes);
app.get('/500',errorController.get500);
app.use(errorController.get404);

app.use((error, req, res, next) => {
    res.status(500).render('error/500',{
            pageTitle: 'Error!',
            path: '/500',
            isAuthenticated: req.session.isLoggedIn
        })
})

mongoose.connect(process.env.MONGODB_URI, {
    useFindAndModify: false,
    useNewUrlParser: true,
    useUnifiedTopology: true
})
    .then(() => {
        console.log('MongoDB has been connected.')
        app.listen(port, () => {
            console.log('App is listening on port: ', port);
        });
    })
    .catch(err => console.log(err));




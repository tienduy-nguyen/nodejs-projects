const express = require('express');
const { check, body } = require('express-validator');
const authController = require('../controllers/auth');
const router = express.Router();
const User = require('../models/user');


router.get('/login', authController.getLogin);

router.post('/logout', authController.postLogout);

router.post('/login',
    [
        check('email')
            .isEmail()
            .withMessage('Please enter a valid email')
            .normalizeEmail(),
        body('password', 'Password has to be valid')
            .isLength({ min: 3 })
            .trim()
    ],
    authController.postLogin);


router.get('/signup', authController.getSignup);


router.post('/signup',
    [
        check('email')
            .isEmail()
            .withMessage('Please enter a valid email')
            .normalizeEmail()
            .custom((email, { req }) => {
                return User.findOne({ email: email })
                    .then(userDoc => {
                        if (userDoc) {
                            return Promise.reject('Email exists already, please pick a different one.');
                        }
                    })
            }),
        check('password')
            .isLength({ min: 3 })
            .trim()
            .withMessage('Password must be at least 3 charaters long')
            .custom((value, { req }) => {
                if (!value.match(/(.*[A-Z])(.*\d.*)/m)) {
                    throw new Error('Password must be contains at least one uppercase character and one digit.')
                }
                return true;
            }),
        body('confirmPassword')
            .trim()
            .custom((value, { req }) => {
                if (value !== req.body.password) {
                    throw new Error('Password not match.')
                }
                return true;
            })
    ],
    authController.postSignup);


router.get('/reset', authController.getReset);

router.post('/reset',
    check('email')
        .isEmail()
        .withMessage('Please enter a valid email')
        .normalizeEmail(),
    authController.postReset);

router.get('/reset/:token', authController.getNewPassword);

router.post('/new-password',
    [
        check('password')
            .isLength({ min: 3 })
            .trim()
            .withMessage('Password must be at least 3 charaters long')
            .custom((value, { req }) => {
                if (!value.match(/(.*[A-Z])(.*\d.*)/m)) {
                    throw new Error('Password must be contains at least one uppercase character and one digit.')
                }
                return true;
            }),
        body('confirmPassword')
            .trim()
            .custom((value, { req }) => {
                if (value !== req.body.password) {
                    throw new Error('Password not match.')
                }
                return true;
            })
    ],
    authController.postNewPassword);

    

module.exports = router;

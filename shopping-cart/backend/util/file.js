const fs = require('fs');

const deleteFile = (filePath) => {
    fs.unlink(filePath, (err) => {
        if (err) {
            console.log('An error occured when trying delete image!',err)
            throw (err);
        }
    });
}

exports.deleteFile = deleteFile;
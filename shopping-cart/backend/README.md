# RESTAPI SAMPLE FOR SHOPING CART

[Live Demo](https://td-shop.herokuapp.com/)

## Technologies

- Nodejs, express for backend
- MongoDB, Mongoose
- Bcrypt Password hashing
- EJS for view template
- Stripe for checkout 
- Multer for uploading
- Nodemailer, Sendgrid for mailer
- Pdfkit for exporting PDF invoice
- Heroku


## .ENV file
- Create `.env` file and add the following information.
  ```
  NODE_ENV=developement
  # For mongodb
  MONGODB_URI=<your link mongodb atlas>

  # For SendGrid
  API_KEY=<your api key sendgrid>

  # For stripe
  stripe_api_pk=<your stripe_pk>
  stripe_api_sk=<your stripe_sk>
  ```

## Quick start

- Install server dependencies and run server

  ```bash
  $ npm install
  $ npm run server
  ```
const User = require('../models/user');
const bcrypt = require('bcryptjs');
const nodemailer = require('nodemailer');
const sendGridTransport = require('nodemailer-sendgrid-transport');
const crypto = require('crypto');
const { validationResult } = require('express-validator');
const dotenv = require('dotenv');
dotenv.config();

const options = {
  auth: {
    api_key: process.env.API_KEY
  }
}
const transporter = nodemailer.createTransport(sendGridTransport(options));

exports.getLogin = (req, res, next) => {
  let message = req.flash('errorLogin');
  if (message.length > 0) {
    message = message[0];
  } else {
    message = null;
  }
  res.render('auth/login', {
    pageTitle: 'Login',
    path: '/login',
    errorMessage: message,
    oldInput: {
      email: '',
      password: ''
    },
    validationErrors: []
  });
};



exports.postLogin = (req, res, next) => {
  const email = req.body.email;
  const password = req.body.password;
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).render('auth/login', {
      path: '/login',
      pageTitle: 'Login',
      errorMessage: errors.array()[0].msg,
      oldInput: {
        email: email,
        password: password
      },
      validationErrors: errors.array()
    });
  }

  User.findOne({ email: email })
    .then(user => {
      if (!user) {
        return res.status(422).render('auth/login', {
          path: '/login',
          pageTitle: 'Login',
          errorMessage: 'Email or password not correct.',
          oldInput: {
            email: email,
            password: password
          },
          validationErrors: [
            { value: email, param: 'email' },
            { value: password, param: 'password' }
          ]
        });
      }
      bcrypt.compare(password, user.password)
        .then(doMatch => {
          if (doMatch) {
            req.session.isLoggedIn = true;
            req.session.user = user;
            return req.session.save(err => {
              if (err) console.log(err);
              res.redirect('/');
            });
          };
          return res.status(422).render('auth/login', {
            path: '/login',
            pageTitle: 'Login',
            errorMessage: 'Email or password not correct.',
            oldInput: {
              email: email,
              password: password
            },
            validationErrors: [
              { value: email, param: 'email' },
              { value: password, param: 'password' }
            ]
          });
        })
    })
    .catch(err => {
      const error = new Error(err);
      error.httpStatusCode = 500;
      return next(error);
    });
};


exports.getSignup = (req, res, next) => {
  let message = req.flash('errorSignup');
  if (message.length > 0) {
    message = message[0];
  } else {
    message = null;
  }
  res.render('auth/signup', {
    path: '/signup',
    pageTitle: 'Signup',
    errorMessage: message,
    oldInput: {
      email: '',
      password: '',
      confirmPassword: ''
    },
    validationErrors: []
  });
};


exports.postSignup = (req, res, next) => {
  const email = req.body.email;
  const password = req.body.password;
  const errors = validationResult(req);
  // console.log(errors.array())
  if (!errors.isEmpty()) {
    return res.status(422).render('auth/signup', {
      path: '/signup',
      pageTitle: 'Signup',
      errorMessage: errors.array()[0].msg,
      oldInput: { email: email, password: password, confirmPassword: req.body.confirmPassword },
      validationErrors: errors.array()
    });
  }
  bcrypt.hash(password, 12)
    .then(hashedPassword => {
      const user = new User({
        email: email,
        password: hashedPassword,
        cart: { items: [] }
      });
      return user.save();
    })
    .then(result => {
      res.redirect('/login');
      return transporter.sendMail({
        from: `Supporter Shopping Cart <noreply@adev42.com>`,
        to: email,
        subject: 'Request signup',
        html: `
              <h1>You requested a signup in shopping cart website</h1>
              <p>Click the link below to confirm your request</p>
              <a href="http://localhost:3000">Reset Password</a>
              <p>This link will expire in <strong>1h</strong></p>
              `
      });
    })
    .catch(err => {
      const error = new Error(err);
      error.httpStatusCode = 500;
      return next(error);
    });
};


exports.postLogout = (req, res, next) => {
  req.session.destroy(() => {
    res.redirect('/');
  })
};

exports.getReset = (req, res, next) => {
  let message = req.flash('errorReset');
  if (message.length > 0) {
    message = message[0];
  } else {
    message = null;
  }
  res.render('auth/reset', {
    path: '/reset',
    pageTitle: 'Reset password',
    errorMessage: message
  });
}


exports.postReset = (req, res, next) => {

  crypto.randomBytes(32, (err, buffet) => {
    if (err) {
      console.log(err);
      return res.redirect('/reset');
    }
    const token = buffet.toString('hex');
    User.findOne({ email: req.body.email })
      .then(user => {
        if (!user) {
          req.flash('errorReset', 'No account with that email found.');
          return res.redirect('/reset');
        }
        user.resetToken = token;
        user.resetTokenExpiration = Date.now() + 3600000;
        return user.save();
      })
      .then(result => {
        res.redirect('/');
        return transporter.sendMail({
          from: `Supporter Shopping Cart <noreply@adev42.com>`,
          to: req.body.email,
          subject: 'Password reset',
          text: 'Hi,',
          html: `
          <h1>You requested a password reset</h1>
          <p>Click the link below to reset your password</p>
          <a href="http://localhost:3000/reset/${token}">Reset Password</a>
          <p>This link will expire in <strong>1h</strong></p>
          `
        });
      })
      .catch(err => {
        const error = new Error(err);
        error.httpStatusCode = 500;
        return next(error);
      });
  })
};


exports.getNewPassword = (req, res, next) => {
  const token = req.params.token;
  User.findOne({
    resetToken: token,
    resetTokenExpiration: { $gt: Date.now() }
  })
    .then(user => {
      let message = req.flash('errorNewPassword');
      if (message.length > 0) {
        message = message[0];
      } else {
        message = null;
      }
      res.render('auth/new-password', {
        path: '/new-password',
        pageTitle: 'Reset Password',
        errorMessage: message,
        userId: user._id.toString(),
        passwordToken: token,
        oldInput:{
          password:'',
          confirmPassword:''
        }
      });
    })
    .catch(err => {
      const error = new Error(err);
      error.httpStatusCode = 500;
      return next(error);
    });
}


exports.postNewPassword = (req, res, next) => {
  const token = req.params.token;
  const password = req.body.password;
  const confirmPassword = req.body.confirmPassword;
  const userId = req.body.userId;
  const passwordToken = req.body.passwordToken;
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).render('auth/new-password', {
      path: '/reset/' + token,
      pageTitle: 'Reset Password',
      userId: userId,
      passwordToken: token,
      errorMessage: errors.array()[0].msg,
      oldInput: { password: password, confirmPassword: req.body.confirmPassword },
      validationErrors: errors.array()
    });
  }

  let resetUser;
  User.findOne({
    resetToken: passwordToken,
    resetTokenExpiration: { $gt: Date.now() },
    _id: userId
  })
    .then(user => {
      if (!user) {
        return res.status(422).render('auth/login', {
          path: '/reset/' + token,
          pageTitle: 'Reset Password',
          userId: userId,
          passwordToken: token,
          errorMessage: 'Your link is not valid or expired. You must send a new one.',
          oldInput: { password: password, confirmPassword: req.body.confirmPassword },
          validationErrors: [{ param: 'password' }, { param: 'confirmPassword' }]
        });
      }
      if (password !== confirmPassword) {
        return res.status(422).render('auth/login', {
          path: '/reset/'+token,
          pageTitle: 'Reset Password',
          userId: userId,
          passwordToken: token,
          errorMessage: 'Password not match',
          oldInput: { password: password, confirmPassword: req.body.confirmPassword },
          validationErrors: [{ param: 'password' }, { param: 'confirmPassword' }]
        });
      }

      resetUser = user;
      return bcrypt.hash(password, 12);
    })
    .then(hashedPassword => {
      resetUser.password = hashedPassword;
      resetUser.resetToken = undefined;
      resetUser.resetTokenExpiration = undefined;
      return resetUser.save();
    })
    .then(result => {
      res.redirect('/login');
    })
    .catch(err => {
      const error = new Error(err);
      error.httpStatusCode = 500;
      return next(error);
    });

};


